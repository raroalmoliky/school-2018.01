class StringStorage:
    def __init__(self):
        self._id_to_str = dict()
        self._str_to_id = dict()

    def get_index(self, data):
        if data in self._str_to_id:
            return self._str_to_id[data]
        else:
            index = len(self._str_to_id)
            self._id_to_str[index] = data
            self._str_to_id[data] = index
            return index

    def get_file_path(self, id):
        return self._id_to_str[id]
